require 'spec_helper'

RSpec.describe FieldawareApi do
  after do
    FieldawareApi.reset
  end

  describe ".client" do
    it "should be a FieldawareApi::Client" do
      expect(FieldawareApi.client).to be_a(FieldawareApi::Client)
    end
  end

  describe '#api_key' do
    it 'should return the default api key' do
      expect(FieldawareApi.api_key).to eq(FieldawareApi::Configuration::DEFAULT_API_KEY)
    end
  end

  describe '#api_key=' do
    it 'should set the api key' do
      FieldawareApi.api_key = 'test'
      expect(FieldawareApi.api_key).to eq('test')
    end
  end

  describe '#api_version' do
    it 'should return the default api version' do
      expect(FieldawareApi.api_version).to eq(FieldawareApi::Configuration::DEFAULT_API_VERSION)
    end
  end

  describe '#api_version=' do
    it 'should set the api_version' do
      FieldawareApi.api_version = '/test'
      expect(FieldawareApi.api_version).to eq('/test')
    end
  end

  describe '#adapter' do
    it 'should return the default adapter' do
      expect(FieldawareApi.adapter).to eq(FieldawareApi::Configuration::DEFAULT_ADAPTER)
    end
  end

  describe '#adapter=' do
    it 'should set the adapter' do
      FieldawareApi.adapter = :typhoeus
      expect(FieldawareApi.adapter).to eq(:typhoeus)
    end
  end

  describe '#endpoint' do
    it 'should return the default endpoint' do
      expect(FieldawareApi.endpoint).to eq(FieldawareApi::Configuration::DEFAULT_ENDPOINT)
    end
  end

  describe '#endpoint=' do
    it 'should set the endpoint' do
      FieldawareApi.endpoint = 'http://www.google.com'
      expect(FieldawareApi.endpoint).to eq('http://www.google.com')
    end
  end

  describe '#configure' do
    FieldawareApi::Configuration::VALID_OPTIONS_KEYS.each do |key|

      it "should set the #{key}" do
        FieldawareApi.configure do |config|
          config.send("#{key}=", key)
          expect(FieldawareApi.send(key)).to eq(key)
        end
      end
    end
  end
end

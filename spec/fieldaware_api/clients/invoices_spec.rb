require 'spec_helper'

RSpec.describe FieldawareApi::Client::Invoices do
  before do
    @client = FieldawareApi::Client.new(:api_key => api_key)
  end

  describe '#invoices' do
    it 'should contains items per page' do
      VCR.use_cassette('invoices') do
        per_page = 4
        items = @client.invoices(:pageSize => per_page)
        expect(items).to be_a(Array)
        expect(items.size).to eq(per_page)
      end
    end
    it 'should contains items from start date with pages' do
      VCR.use_cassette('invoices-date-pages') do
        dt = Time.new(2015, 12, 28)
        params = {:start_date => dt, :page => 0}
        items = []
        2.times do
          items += @client.invoices(params)
          params[:page] += 1
        end
        expect(items.size).to eq(8)
      end
    end
    it 'should select from start date' do
      VCR.use_cassette('invoices_start') do
        dt = Time.new(2015, 12, 25)
        result = @client.invoices(:start_date => dt)
        expect(result.size).to eq(8)
      end
    end
    it 'should select from start to end date' do
      VCR.use_cassette('invoices_start_end') do
        dt = Time.new(2015, 12, 10)
        result = @client.invoices(:start_date => dt, :end_date => dt)
        expect(result.size).to eq(5)
      end
    end
  end

  describe '#invoice' do
    context 'found and' do
      before do
        VCR.use_cassette('invoice') do
          @uuid = 'fe60aa847dd94dc283302dbe4f7a4eeb'
          @invoice = @client.invoice(@uuid)
        end
      end
      it 'should be a hash with the same uuid' do
        expect(@invoice).to be_a(Hash)
        expect(@invoice.uuid).to eq(@uuid)
      end
    end
    it 'should return an empty invoice if not found' do
      VCR.use_cassette('invoice-not-found') do
        result = @client.invoice(0)
        expect(result).to be_a(Hash)
        expect(result.id).to be_nil
      end
    end
  end

end

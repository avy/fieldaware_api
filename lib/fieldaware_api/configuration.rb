module FieldawareApi
  module Configuration

    VALID_OPTIONS_KEYS = [
      :api_key,
      :api_version,
      :adapter,
      :endpoint
    ].freeze

    DEFAULT_API_KEY = nil

    DEFAULT_API_VERSION = ''.freeze

    # Use the default Faraday adapter.
    DEFAULT_ADAPTER = Faraday.default_adapter

    DEFAULT_ENDPOINT = 'https://api.fieldaware.net'.freeze

    DEFAULT_FORMAT = :json

    attr_accessor *VALID_OPTIONS_KEYS

    # Convenience method to allow configuration options to be set in a block
    def configure
      yield self
    end

    def options
      VALID_OPTIONS_KEYS.inject({}) do |option, key|
        option.merge!(key => send(key))
      end
    end

    # When this module is extended, reset all settings.
    def self.extended(base)
      base.reset
    end

    def reset
      self.api_key     = DEFAULT_API_KEY
      self.api_version = DEFAULT_API_VERSION
      self.endpoint    = DEFAULT_ENDPOINT
      self.adapter     = DEFAULT_ADAPTER
    end
  end
end

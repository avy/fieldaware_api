module FieldawareApi
  class Client
    module Invoices

      def invoices(params = {})
        uuids = get_uuids("invoice/", "Invoice", params)
        uuids.map{|c|invoice(c)} if uuids
      end

      def invoice(id, params = {})
        response = get("invoice/#{id}", params)
      end

    end
  end
end

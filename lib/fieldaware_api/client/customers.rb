module FieldawareApi
  class Client
    module Customers

      def customers(params = {})
        uuids = get_uuids("customer/", "Customer", params)
        uuids.map{|c|customer(c)} if uuids
      end

      def customer(id, params = {})
        get("customer/#{id}", params)
      end

    end
  end
end
